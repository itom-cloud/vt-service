package com.d2d.vt.controller

import com.d2d.vt.dto.VehicleLocationRequest
import com.d2d.vt.dto.VehicleRegRequest
import com.d2d.vt.queues.QueueProducer
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/vehicles")
@Validated
class VehicleController(private val queueProducer: QueueProducer) {

    @PostMapping("/")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun register(@RequestBody vehicleRegRequest: VehicleRegRequest): ResponseEntity<Any> {
        return queueProducer.sendVehicleReg(vehicleRegRequest).let {
            ResponseEntity.status(HttpStatus.NO_CONTENT).build<Any>()
        }
    }

    @PostMapping("/{id}/locations")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun location(@PathVariable id: String,
                 @RequestBody vehicleLocationRequest: VehicleLocationRequest): ResponseEntity<Any> {
        return queueProducer.sendVehicleLocation(id, vehicleLocationRequest).let {
            ResponseEntity.status(HttpStatus.NO_CONTENT).build<Any>()
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deregister(@PathVariable id: String): ResponseEntity<Any> {
        return queueProducer.sendVehicleDeReg(id).let {
            ResponseEntity.status(HttpStatus.NO_CONTENT).build<Any>()
        }
    }


}
