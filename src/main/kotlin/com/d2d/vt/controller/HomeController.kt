package car.hey.crm.controllers

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PostMapping

@RestController
@RequestMapping("/home")
class HomeController {
	
	@PostMapping
	fun printlog(){
		println("Home page called")
	}

	@GetMapping("/test", produces = [MediaType.APPLICATION_JSON_VALUE])
	fun home() = object {
		val title = "Notice"
		val description = "Welcome to D2D VT"
	}
}
