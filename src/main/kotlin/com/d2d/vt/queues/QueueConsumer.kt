package com.d2d.vt.queues

import com.d2d.vt.Constants
import com.d2d.vt.dto.VehicleLocationRequest
import com.d2d.vt.dto.VehicleRegRequest
import com.d2d.vt.model.VehicleLocation
import com.d2d.vt.model.VehicleReg
import com.d2d.vt.service.VehicleLocationService
import com.d2d.vt.service.VehicleRegService
import com.d2d.vt.toLocalDateTime
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class QueueConsumer(val vehicleRegService: VehicleRegService, val vehicleLocationService: VehicleLocationService,
                    val mapper: ObjectMapper) {

    @JmsListener(destination = Constants.QUEUE_VEHICLE_REG)
    fun processRegistration(message: String) {
        val vehicleRegRequest: VehicleRegRequest = mapper.readValue(message, VehicleRegRequest::class.java)
        vehicleRegService.register(VehicleReg(vehicleId = vehicleRegRequest.id, createdOn = LocalDateTime.now()))
    }

    @JmsListener(destination = Constants.QUEUE_VEHICLE_DEREG)
    fun processDeRegistration(message: String) {
        vehicleRegService.deRegister(message)
    }

    @JmsListener(destination = Constants.QUEUE_VEHICLE_LOCATION)
    fun processLocation(message: String) {
        val vlr: VehicleLocationRequest = mapper.readValue(message, VehicleLocationRequest::class.java)
        vehicleLocationService.updateLocation(VehicleLocation(null, vlr.vehicleId, vlr.lat, vlr.lng, vlr.at.toLocalDateTime()))
    }

}