package com.d2d.vt.queues

import org.springframework.jms.core.JmsTemplate
import com.d2d.vt.dto.VehicleRegRequest
import com.d2d.vt.Constants
import com.d2d.vt.dto.VehicleLocationRequest
import org.springframework.stereotype.Service

@Service
class QueueProducer(val jmsTemplate: JmsTemplate) {

    fun sendVehicleReg(vehicleRegRequest: VehicleRegRequest) {
        send(Constants.QUEUE_VEHICLE_REG, vehicleRegRequest)
    }

    fun sendVehicleLocation(vehicleId: String, vehicleLocationRequest: VehicleLocationRequest) {
        vehicleLocationRequest.vehicleId = vehicleId
        send(Constants.QUEUE_VEHICLE_LOCATION, vehicleLocationRequest)
    }

    fun sendVehicleDeReg(id: String) {
        send(Constants.QUEUE_VEHICLE_DEREG, id)
    }

    private fun send(destination: String, message: Any) {
        jmsTemplate.convertAndSend(destination, message)
    }
}