package com.d2d.vt

object Constants {
	const val QUEUE_VEHICLE_REG: String = "vehicle_reg"
	const val QUEUE_VEHICLE_DEREG: String = "vehicle_dereg"
	const val QUEUE_VEHICLE_LOCATION: String = "vehicle_location"
}