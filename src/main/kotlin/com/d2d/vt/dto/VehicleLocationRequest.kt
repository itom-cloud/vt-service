package com.d2d.vt.dto

data class VehicleLocationRequest(var vehicleId: String, var lat: Double, var lng: Double, var at: String) {
}