package com.d2d.vt.config

import org.springframework.context.annotation.Bean
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ComponentScan

@Configuration
@ComponentScan(basePackages = ["com.d2d.vt"])
class AppConfig {

	@Bean
	public fun docConfig(): OpenAPI {
		return OpenAPI().components(Components()).info(
						Info().title("Vehicle Tracking API Docs").description("Vehicle Tracking API REST Documentation")
		);
	}
}


