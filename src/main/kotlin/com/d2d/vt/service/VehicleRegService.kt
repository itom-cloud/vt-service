package com.d2d.vt.service

import org.springframework.stereotype.Service
import com.d2d.vt.repository.VehicleRegRepository
import com.d2d.vt.model.VehicleReg
import java.time.LocalDateTime
import org.springframework.jms.core.JmsTemplate
import org.springframework.jms.core.MessageCreator
import org.springframework.jms.annotation.JmsListener
import com.d2d.vt.dto.VehicleRegRequest
import com.d2d.vt.model.VehicleLocation

@Service
class VehicleRegService(val vehicleRegRepository: VehicleRegRepository) {


    fun register(vehicleReg: VehicleReg) {
        vehicleRegRepository.save(vehicleReg)
    }

    fun deRegister(vehicleId: String) {
        vehicleRegRepository.deleteByVehicleId(vehicleId)
    }

}