package com.d2d.vt.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.LocalDateTime

@Document(collection = "vehicle_reg")
data class VehicleReg(@Id val id: String?=null, val vehicleId:String, val createdOn: LocalDateTime) {

}