package com.d2d.vt.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document("vehicle_location")
data class VehicleLocation(@Id var id: String? = null, var vehicleId: String, var lat: Double, var lng: Double, var at: LocalDateTime) {

}