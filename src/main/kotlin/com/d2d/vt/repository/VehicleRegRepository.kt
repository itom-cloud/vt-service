package com.d2d.vt.repository

import com.d2d.vt.model.VehicleReg
import org.springframework.data.mongodb.repository.MongoRepository

interface VehicleRegRepository : MongoRepository<VehicleReg, String> {

     fun deleteByVehicleId(vehicleId: String)
}