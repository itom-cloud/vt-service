package com.d2d.vt.repository

import com.d2d.vt.model.VehicleLocation
import org.springframework.data.mongodb.repository.MongoRepository

interface VehicleLocationRepository : MongoRepository<VehicleLocation, String> {

}