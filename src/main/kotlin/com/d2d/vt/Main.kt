package com.d2d.vt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.time.LocalDateTime

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}


fun String.toLocalDateTime(): LocalDateTime = LocalDateTime.parse(this)