package com.d2d.vt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication(scanBasePackages = "com.d2d.vt")
@EnableJms 
public class Application {

	public static void maine(String... arg) {
		SpringApplication.run(Application.class, arg);
	}

	

}
